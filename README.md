# Imagen Docker con PHP con la última version estable y Apache 2.4

Esta imagen tiene como base [php](https://hub.docker.com/_/php):apache.

Además tiene las siguientes personalizaciones:

- Creación automática de certificado para conexiones por https.
- Creación de usuario y grupo con id 1000 con el cual correrá Apache.
- Añadido a sudores el usuario así como al grupo Admin.
- Configuración por defecto para entorno de desarrollo.

## Configuraciones de PHP

- Configuración regional: Europe/Madrid.
- Extensiones añadidas: **pdo_pgsql**


## Creación de la imagen

`docker build --compress --force-rm --no-cache --pull -t registry.gitlab.com/dasua/php-apache:latest .`

## Subir imagen a GitLab

`docker push registry.gitlab.com/dasua/php-apache`

## Inicio de la imagen

#### Variables de entorno

Estas son las variables de entorno que se pueden configurar para lanzar el contenedor:

- **APP_ENV**: Entorno que se está utilizando. Se utiliza para indicar el entorno de ejecución de CodeIgniter.
  - Valor por defecto: _development_.
- **APP_BASE_DIR**: Ruta donde reside la aplicación en el container.
  - Valor por defecto: _/app_
- **APP_SERVER_ADMIN**: Email de configuración de servidor.
  - Valor por defecto: _nomail@example.org_
- **APP_WEB_DOMAIN**: Host a indicar en la url.
  - Valor por defecto: _localhost_
- **APACHE_DOCUMENT_ROOT**: Ruta donde está el document_root de la aplicación.
  - Valor por defecto: _/app/html_

#### Volúmenes

Para poder ejecutar la aplicación, hay que indicar una ruta donde reside la aplicación.

Esta ruta no es la del _APACHE_DOCUMENT_ROOT_, es el directorio padre. Es decir, si hay la siguiente estructura de directorios:
```
~/repos/codigo/miAplicacion/
~/repos/codigo/miAplicacion/html
```

La primera ruta hace referencia al volumen que hay que montar y la segunda es el document_root de Apache.

### Ejecución con Docker

`docker run -d --rm -p 80:80 -p 443:443 -v $PWD:'/app' --env APP_WEB_DOMAIN='localhost' --name myApp 'registry.gitlab.com/dasua/php-apache:latest'`

Con este comando se lanza el contendor con las siguientes opciones:

- Eliminar el contenedor al terminar su ejecución.
- Ejecutar como demonio.
- Exporta el puerto 80.
- Exporta el puerto 443.
- Crea el volumen de forma que el directorio actual va a ser montado como raíz de la aplicación en el contenedor.
- Define el host de la url (https://localhost).
- Asigna el nombre al contenedor (myApp).
- Utiliza la imagen **registry.gitlab.com/dasua/php-apache** en el versión **latest**

Para poder entrar en la línea de comandos del contedor, desde la terminal ejecutar el comando:

`docker exec -it myApp /bin/bash`